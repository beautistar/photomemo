//
//  ANSAddTargetViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/12/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

extension UIImage {
    var uncompressedPNGData: NSData      { return UIImagePNGRepresentation(self)!        }
    var highestQualityJPEGNSData: NSData { return UIImageJPEGRepresentation(self, 1.0)!  }
    var highQualityJPEGNSData: NSData    { return UIImageJPEGRepresentation(self, 0.75)! }
    var mediumQualityJPEGNSData: NSData  { return UIImageJPEGRepresentation(self, 0.5)!  }
    var lowQualityJPEGNSData: NSData     { return UIImageJPEGRepresentation(self, 0.25)! }
    var lowestQualityJPEGNSData:NSData   { return UIImageJPEGRepresentation(self, 0.0)!  }
}

class ANSAddTargetViewController: UIViewController,
UITextFieldDelegate{
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var closeBtn : UIButton!
    var appNameImageView: UIImageView!
    var photoImageView: UIImageView!
    var addPhotoBtn: UIButton!
    var endLineView: UIView!
    var fillUpLabel: UILabel!
    var titleTextField: UITextField!
    var goalTextField: UITextField!
    var dueDateTextField: UITextField!
    var titleLine: UIView!
    var goalLine: UIView!
    var dueDateLine: UIView!
    var letGoBtn: UIButton!
    var assets: [DKAsset]?
    var dueDatePicker: UIDatePicker!
    var dueDateBtn: UIButton!
    var dueDateTitleLabel: UILabel!
    var dueDateLabel: UILabel!
    var targetState: String!
    var targetID: String!
    var photoFile: PFFile!
    
    override func viewWillAppear(animated: Bool) {
        
        if targetState == "Edit" {
            
            self.fetchTargetDetail()
        
        }else{
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupAppearance()
    }
    
    func fetchTargetDetail() {
        
        
        let query = PFQuery(className: "Target")
        query.includeKey("owner")
        query.whereKey("objectId", equalTo: self.targetID)
        query.findObjectsInBackgroundWithBlock { (detailObj, NSError) -> Void in
            
            for detail in detailObj! {
                print("Detail \(detail)")

                
                self.photoFile = detail.valueForKey("targetPhoto") as! PFFile
                
                self.photoFile.getDataInBackgroundWithBlock({ (imageData, NSError) -> Void in
                    
                    self.photoImageView.image = UIImage(data: imageData!)
                    
                    
                    }) { (percentDone: Int32) -> Void in
                        
                }
                
                self.titleTextField.text = detail.valueForKey("targetName") as? String
                
                let date = detail.valueForKey("dueDate") as? NSDate
                let formatter = NSDateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
                //self.dueDateLabel.text = "Due date : \(formatter.stringFromDate(date!))"
                self.dueDatePicker.setDate(date!, animated: true)
                
                let goalNumber = detail.valueForKey("goal") as! Double
                self.goalTextField.text = "\(goalNumber)"


                
            }
            
            
        }
        
    }
    
    func setupAppearance() {
        
        let closeImg = UIImage(named: "ans_close_icon")!
        
        closeBtn = UIButton(frame: CGRectMake(screenSize.width * 0.865, screenSize.height * 0.085, closeImg.size.width * 1.35, closeImg.size.height * 1.35))
        closeBtn.setBackgroundImage(closeImg, forState: UIControlState.Normal)
        closeBtn.addTarget(self, action: #selector(ANSAddTargetViewController.closeTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(closeBtn)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - ((appNameImg.size.width * 1.25)/2), screenSize.height * 0.07, appNameImg.size.width * 1.25, appNameImg.size.height * 1.25))
        appNameImageView.image = appNameImg
        self.view.addSubview(appNameImageView)
        
        
        photoImageView = UIImageView(frame: CGRectMake(screenSize.width * 0.05, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width * 0.9, screenSize.height * 0.3))
        photoImageView.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
        photoImageView.layer.cornerRadius = 2.0
        photoImageView.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        photoImageView.layer.borderWidth = 0.75
        photoImageView.contentMode = UIViewContentMode.ScaleAspectFill
        photoImageView.clipsToBounds = true
        photoImageView.userInteractionEnabled = true
        self.view.addSubview(photoImageView)
        
        addPhotoBtn = UIButton(frame: CGRectMake(photoImageView.frame.width * 0.3, (photoImageView.frame.height/2) - (photoImageView.frame.height * 0.15), photoImageView.frame.width * 0.4, photoImageView.frame.height * 0.3))
        addPhotoBtn.setTitle("Add Photo", forState: UIControlState.Normal)
        addPhotoBtn.backgroundColor = UIColor.whiteColor()
        addPhotoBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        addPhotoBtn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        addPhotoBtn.layer.cornerRadius = 3.0
        addPhotoBtn.layer.borderWidth = 0.75
        addPhotoBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        addPhotoBtn.addTarget(self, action: #selector(ANSAddTargetViewController.photoTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        photoImageView.addSubview(addPhotoBtn)
        //self.view.addSubview(addPhotoBtn)
        
        endLineView = UIView(frame: CGRectMake(0, photoImageView.frame.origin.y + photoImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width, 0.5))
        endLineView.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        self.view.addSubview(endLineView)
        
        fillUpLabel = UILabel(frame: CGRectMake(screenSize.width * 0.05, endLineView.frame.origin.y + endLineView.frame.size.height + (screenSize.height * 0.005), screenSize.width * 0.9, screenSize.height * 0.04))
        fillUpLabel.backgroundColor = UIColor.clearColor()
        fillUpLabel.textColor = UIColor.lightGrayColor()
        fillUpLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 16)
        fillUpLabel.text = "Fill up your info"
        self.view.addSubview(fillUpLabel)
        
        
        let titlePlaceholder = NSAttributedString(string: "Title", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        titleTextField = UITextField(frame: CGRectMake(screenSize.width * 0.05, fillUpLabel.frame.origin.y + fillUpLabel.frame.size.height + (screenSize.height * 0.02), screenSize.width * 0.9, screenSize.height * 0.06))
        titleTextField.backgroundColor = UIColor.clearColor()
        titleTextField.attributedPlaceholder = titlePlaceholder
        titleTextField.textColor = UIColor.darkGrayColor()
        titleTextField.delegate = self
        titleTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(titleTextField)
        
        
        
        titleLine = UIView(frame: CGRectMake(screenSize.width * 0.05, titleTextField.frame.origin.y + titleTextField.frame.size.height, screenSize.width * 0.9, 1))
        titleLine.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        self.view.addSubview(titleLine)
        
        
        let goalPlaceholder = NSAttributedString(string: "Note Details", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        goalTextField = UITextField(frame: CGRectMake(screenSize.width * 0.05, titleLine.frame.origin.y + titleLine.frame.size.height + (screenSize.height * 0.02), screenSize.width * 0.9, screenSize.height * 0.06))
        goalTextField.backgroundColor = UIColor.clearColor()
        goalTextField.attributedPlaceholder = goalPlaceholder
        goalTextField.textColor = UIColor.darkGrayColor()
        goalTextField.delegate = self
        goalTextField.keyboardType = UIKeyboardType.DecimalPad
        goalTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(goalTextField)
        
        
        
        goalLine = UIView(frame: CGRectMake(screenSize.width * 0.05, goalTextField.frame.origin.y + goalTextField.frame.size.height, screenSize.width * 0.9, 1))
        goalLine.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        self.view.addSubview(goalLine)

        
        
        dueDateTitleLabel = UILabel(frame: CGRectMake(screenSize.width * 0.05, goalLine.frame.origin.y + goalLine.frame.size.height + (screenSize.height * 0.035), screenSize.width * 0.9, screenSize.height * 0.04))
        dueDateTitleLabel.text = "Date created"
        dueDateTitleLabel.backgroundColor = UIColor.clearColor()
        dueDateTitleLabel.textColor = UIColor.lightGrayColor()
        dueDateTitleLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 13)
        self.view.addSubview(dueDateTitleLabel)
        
        
        dueDatePicker = UIDatePicker(frame: CGRectMake(screenSize.width * 0.05, dueDateTitleLabel.frame.origin.y + dueDateTitleLabel.frame.size.height + (screenSize.height * 0.01), screenSize.width * 0.9, screenSize.height * 0.07))
        dueDatePicker.datePickerMode = UIDatePickerMode.Date
        dueDatePicker.setValue(UIColor.darkGrayColor(), forKeyPath: "textColor")

        self.view.addSubview(dueDatePicker)
        

        letGoBtn = UIButton(frame: CGRectMake(screenSize.width * 0.3, dueDatePicker.frame.origin.y + dueDatePicker.frame.size.height + (screenSize.height * 0.035), screenSize.width * 0.4, screenSize.height * 0.07))
        letGoBtn.setTitle("Let's Go", forState: UIControlState.Normal)
        letGoBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        letGoBtn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        letGoBtn.layer.cornerRadius = 3.0
        letGoBtn.layer.borderWidth = 0.75
        letGoBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        letGoBtn.layer.borderWidth = 0.75
        letGoBtn.addTarget(self, action: #selector(ANSAddTargetViewController.saveTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(letGoBtn)
        
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(ANSAddTargetViewController.touchViewTapped(_:)))
        self.view.addGestureRecognizer(viewTap)
        
    }
    
    
    
    struct Demo {
        static let titles = [
            ["Pick All", "Pick photos only", "Pick videos only"],
            ["Pick All (only photos or videos)"],
            ["Take a picture"],
            ["Hides camera"]
        ]
        static let types: [DKImagePickerControllerAssetType] = [.allAssets, .allPhotos, .allVideos]
    }
    
    
    func photoTapped(sender : UIButton) {
        //let photoPicker = DKImagePickerController()
        
        let assetType = Demo.types[0]
        let allowMultipleType = false
        let sourceType: DKImagePickerControllerSourceType = [.Camera, .Photo]
        
        showImagePickerWithAssetType(assetType,
            allowMultipleType: allowMultipleType,
            sourceType: sourceType)
    }
    
    func showImagePickerWithAssetType(assetType: DKImagePickerControllerAssetType,
        allowMultipleType: Bool = true,
        sourceType: DKImagePickerControllerSourceType = [.Camera, .Photo]) {
            
            let pickerController = DKImagePickerController()
            pickerController.assetType = assetType
            pickerController.allowMultipleTypes = allowMultipleType
            pickerController.sourceType = sourceType
            pickerController.singleSelect = true
            
            pickerController.didCancel = {
                print("didCancel")
            }
            
            pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
                print("didSelectAssets")
                print(assets.map({ $0.url}))
                
                self.assets = assets
                
                let asset = self.assets![0]
                self.photoImageView.image = asset.fullResolutionImage
                self.addPhotoBtn.setTitle("Other Photo", forState: UIControlState.Normal)
                
            }
            
            self.presentViewController(pickerController, animated: true) {}
    }
    
    func touchViewTapped(sender : UITapGestureRecognizer) {
        
        titleTextField!.resignFirstResponder()
        goalTextField!.resignFirstResponder()
        
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        
    }
    
    func closeTapped(sender:UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == titleTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
        if textField == goalTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
        
        
    }

    func textFieldDidEndEditing(textField: UITextField) {
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        titleTextField!.resignFirstResponder()
        goalTextField!.resignFirstResponder()
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        return true
    }
    
    func saveTapped(sender : UIButton) {
        
        
        if targetState == "Edit" {
            if photoImageView.image != nil && titleTextField.text != "" && goalTextField.text != ""{
                
                self.view.showActivityViewWithLabel("Updating...")
                            
                let targetQuery = PFQuery(className: "Target")
                targetQuery.whereKey("objectId", equalTo: self.targetID)
                targetQuery.findObjectsInBackgroundWithBlock({ (targetObj, NSError) -> Void in
                    
                    for targetObject in targetObj! {
                        
                        targetObject["targetName"] = self.titleTextField.text
                        let goalDouble = Double(self.goalTextField.text!)
                        targetObject["goal"] = goalDouble
                        targetObject["owner"] = PFUser.currentUser()
                        targetObject["dueDate"] = self.dueDatePicker.date
                        
                        let imageData = self.photoImageView.image!.mediumQualityJPEGNSData
                        let imageFile = PFFile(name:"targetPhoto.png", data:imageData)
                        targetObject["targetPhoto"] = imageFile
                        targetObject.saveInBackgroundWithTarget(self, selector: #selector(ANSAddTargetViewController.uploadingProgress));
                        
                    }
                    
                    
                })
                
                
            }else{
                presentPopupView(createPhotoPopupview())
            }
        }else{
            if photoImageView.image != nil && titleTextField.text != "" && goalTextField.text != ""{
                
                self.view.showActivityViewWithLabel("Uploading...")
                
                let targetObject = PFObject(className: "Target")
                
                targetObject["targetName"] = self.titleTextField.text
                let goalDouble = Double(self.goalTextField.text!)
                targetObject["goal"] = goalDouble
                targetObject["owner"] = PFUser.currentUser()
                targetObject["dueDate"] = self.dueDatePicker.date
                targetObject["current"] = 0.0
                
                let imageData = self.photoImageView.image!.mediumQualityJPEGNSData
                let imageFile = PFFile(name:"targetPhoto.png", data:imageData)
                targetObject["targetPhoto"] = imageFile
                targetObject.saveInBackgroundWithTarget(self, selector: #selector(ANSAddTargetViewController.uploadingProgress));
                
                
            }else{
                presentPopupView(createPhotoPopupview())
            }
        }
        
        
        
    }
    
    func uploadingProgress() {
        
        self.view.hideActivityView()
        
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
        
    }
    
    func createPhotoPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(10, 40, 180, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont.systemFontOfSize(13)
        popupLabel.text = "Please fill up all field."
        popupLabel.numberOfLines = 2
        popupView.addSubview(popupLabel)
        
        // Close button
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSAddTargetViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    
    
    func touchClose() {
        dismissPopupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
