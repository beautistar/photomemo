//
//  ANSAddSavingViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/12/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

class ANSAddSavingViewController: UIViewController ,
UITableViewDelegate,
UITableViewDataSource,
UITextFieldDelegate,
UIActionSheetDelegate{

    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var appNameImageView: UIImageView!
    var closeBtn: UIButton!
    var historyTableView: UITableView!
    var endLineView: UIView!
    var moneyTextField: UITextField!
    var addBtn: UIButton!
    var moneyList: NSMutableArray = []
    var dateTimeList: NSMutableArray = []
    var savingList: NSMutableArray = []
    var targetObjectID: String!
    var targetObject: PFObject!
    var savingObj: PFObject!
    
    override func viewWillAppear(animated: Bool) {
        self.fetchHistory()
    }
    
    func fetchHistory() {
        let moneyArray: NSMutableArray = []
        let dateTimeArray: NSMutableArray = []
        let savingArray: NSMutableArray = []
        
        let historyQuery = PFQuery(className: "Saving")
        historyQuery.includeKey("targetObj")
        historyQuery.orderByDescending("createdAt")
        historyQuery.whereKey("targetObj", equalTo: targetObject)
        historyQuery.findObjectsInBackgroundWithBlock { (histories, NSError) -> Void in
            
            for historyObj in histories! {
                moneyArray.addObject(historyObj.valueForKey("amount")!)
                dateTimeArray.addObject(historyObj.valueForKey("createdAt")! as! NSDate)
                savingArray.addObject(historyObj)
            }
            
            self.moneyList = moneyArray
            self.dateTimeList = dateTimeArray
            self.savingList = savingArray
            self.historyTableView.reloadData()
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupAppearance()
    }
    
    func setupAppearance() {
        
        let closeImg = UIImage(named: "ans_close_icon")!
        
        closeBtn = UIButton(frame: CGRectMake(screenSize.width * 0.865, screenSize.height * 0.085, closeImg.size.width * 1.35, closeImg.size.height * 1.35))
        closeBtn.setBackgroundImage(closeImg, forState: UIControlState.Normal)
        closeBtn.addTarget(self, action: #selector(ANSAddSavingViewController.closeTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(closeBtn)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - ((appNameImg.size.width * 1.25)/2), screenSize.height * 0.07, appNameImg.size.width * 1.25, appNameImg.size.height * 1.25))
        appNameImageView.image = appNameImg
        self.view.addSubview(appNameImageView)
        
        historyTableView = UITableView(frame: CGRectMake(0, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width, screenSize.height * 0.75))
        historyTableView.delegate = self
        historyTableView.dataSource = self
        historyTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        let historyNibName = UINib(nibName: "ANSHistoryCell", bundle:nil)
        historyTableView.registerNib(historyNibName, forCellReuseIdentifier: "HistoryCell")
        
        self.view.addSubview(historyTableView)
        
        addBtn = UIButton(frame: CGRectMake(screenSize.width * 0.72, historyTableView.frame.origin.y + historyTableView.frame.size.height + (screenSize.height * 0.012), screenSize.width * 0.25, screenSize.height * 0.06))
        addBtn.setTitle("Add it!!!", forState: UIControlState.Normal)
        addBtn.layer.borderWidth = 1
        addBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        addBtn.layer.cornerRadius = 2.0
        addBtn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        addBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.02)
        addBtn.addTarget(self, action: #selector(ANSAddSavingViewController.addTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(addBtn)
        
        
        let moneyPlaceholder = NSAttributedString(string: "Enter your money...", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        
        moneyTextField = UITextField(frame: CGRectMake(screenSize.width * 0.03, historyTableView.frame.origin.y + historyTableView.frame.size.height + (screenSize.height * 0.012), screenSize.width * 0.65, screenSize.height * 0.06))
        moneyTextField.attributedPlaceholder = moneyPlaceholder
        moneyTextField.backgroundColor = UIColor.clearColor()
        moneyTextField.delegate = self
        moneyTextField.keyboardType = UIKeyboardType.DecimalPad
        moneyTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.02)
        self.view.addSubview(moneyTextField)
        
        endLineView = UIView(frame: CGRectMake(screenSize.width * 0.03, moneyTextField.frame.origin.y + moneyTextField.frame.size.height + 1, screenSize.width * 0.65, 0.75))
        endLineView.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        self.view.addSubview(endLineView)
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(ANSAddSavingViewController.touchViewTapped(_:)))
        self.view.addGestureRecognizer(viewTap)
        
        
    }
    
    func touchViewTapped(sender : UITapGestureRecognizer) {
        
        moneyTextField!.resignFirstResponder()
        
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        
    }
    
    func addTapped(sender:UIButton) {
        self.moneyTextField.resignFirstResponder()
        
        if self.moneyTextField.text != "" {
            
            let savingObject = PFObject(className: "Saving")
            savingObject["targetObj"] = self.targetObject
            savingObject["amount"] = Double(self.moneyTextField.text!)
            savingObject["targetOwner"] = PFUser.currentUser()
            savingObject.saveInBackgroundWithBlock({ (success, NSError) -> Void in
                
                
                let targetQuery = PFQuery(className: "Target")
                targetQuery.getObjectInBackgroundWithId(self.targetObjectID, block: { (targetObj, NSError) -> Void in
                    
                    let currentNumber = Double(targetObj?.valueForKey("current") as! Double)
                    targetObj!["current"] = currentNumber + Double(self.moneyTextField.text!)!
                    targetObj!.saveInBackgroundWithBlock({ (success, NSError) -> Void in
                        
                        self.fetchHistory()
                        self.view.frame = CGRectMake(0, 0, self.screenSize.width, self.screenSize.height)
                        self.moneyTextField.text = ""
                        
                    })
                    
                    
                    
                })
                
            })
            
        }else{
            let popupView = self.createPopupview()
            
            self.presentPopupView(popupView)
        }
        
    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(20, 40, 160, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont.systemFontOfSize(16)
        popupLabel.text = "Please fill up your money."
        popupView.addSubview(popupLabel)
        
        // Close button
        //let button = UIButton.buttonWithType(.System)
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSAddSavingViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    
    
    
    func touchClose() {
        dismissPopupView()
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        self.view.frame = CGRectMake(0, -210, screenSize.width, screenSize.height)
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.moneyTextField.resignFirstResponder()
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
    }
    
    func closeTapped(sender:UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.moneyList.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let historyCell = tableView.dequeueReusableCellWithIdentifier("HistoryCell") as! ANSHistoryCell
        
        if self.moneyList.count != 0 {
            historyCell.moneyLabel.text = String("$\(self.moneyList[indexPath.row])")
            
            let date = self.dateTimeList[indexPath.row] as? NSDate
            let formatter = NSDateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
            
            historyCell.dateTimeLabel.text = formatter.stringFromDate(date!)
            
            historyCell.actionBtn.tag = indexPath.row
            
            historyCell.actionBtn.addTarget(self, action: #selector(ANSAddSavingViewController.actionBtn(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            
        }
        
        
        return historyCell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return screenSize.height * 0.13
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func actionBtn(sender : UIButton) {

        savingObj = self.savingList[sender.tag] as! PFObject
        let actionSheet = UIActionSheet(title: "Choose Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Edit", "Delete")
        
        actionSheet.showInView(self.view)
        
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            print("Edit")
            
            let editSavingView = ANSEditSavingViewController();
            editSavingView.savingID = self.savingObj.valueForKey("objectId") as! String
            self.presentViewController(editSavingView, animated: true, completion: { () -> Void in
                
            })
            
        case 2:
            print("Delete")
            
            var savingMoney: Double!
            
            let savingQuery = PFQuery(className: "Saving")
            savingQuery.includeKey("targetObj")
            savingQuery.whereKey("objectId", equalTo: savingObj.valueForKey("objectId")!)
            savingQuery.findObjectsInBackgroundWithBlock({ (saving, NSError) -> Void in
                
                for savingObject in saving! {
                    
                    savingMoney = Double(savingObject.valueForKey("amount") as! Double)
                    
                    savingObject.deleteInBackgroundWithBlock({ (sucess, NSError) -> Void in
                        
                        let targetQuery = PFQuery(className: "Target")
                        targetQuery.whereKey("objectId", equalTo: (savingObject.valueForKey("targetObj")?.valueForKey("objectId"))!)
                        targetQuery.findObjectsInBackgroundWithBlock({ (target, NSError) -> Void in
                            
                            for targetObj in target! {
                                let moneyNumber = Double(targetObj.valueForKey("current") as! Double) - savingMoney
                                targetObj["current"] = moneyNumber
                                targetObj.saveInBackgroundWithBlock({ (success, NSError) -> Void in
                                    
                                    self.fetchHistory()
                                    
                                })
                            }
                            
                            
                        })
                        
                        
                        
                        
                    })
                }
                
            })
            
        default:
            print("Default")
            //Some code here..
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
