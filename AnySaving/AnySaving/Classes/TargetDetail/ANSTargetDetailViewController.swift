//
//  ANSTargetDetailViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/15/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

class ANSTargetDetailViewController: UIViewController,
UIActionSheetDelegate{
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var appNameImageView: UIImageView!
    var closeBtn: UIButton!
    var targetImageView: UIImageView!
    var targetBound: UIView!
    var targetNameLabel: UILabel!
    var dueDateLabel: UILabel!
    var moreBtn: UIButton!
    var currentLabel: UILabel!
    var currentNumberLabel: UILabel!
    var goalLabel: UILabel!
    var goalNumberLabel: UILabel!
    var remainLabel: UILabel!
    var remainNumberLabel: UILabel!
    var endGoalLineView: UIView!
    var halfLine: UIView!
    var targetObject: PFObject!
    var targetID: String!
    var detailObject: PFObject!
    var photoFile: PFFile!
    var addBtn: UIButton!
    var addLabel: UILabel!
    
    
    override func viewWillAppear(animated: Bool) {
        self.fetchTargetDetail()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupAppearance()
    }
    
    func fetchTargetDetail() {
        
        let query = PFQuery(className: "Target")
        query.includeKey("owner")
        query.whereKey("objectId", equalTo: self.targetID)
        query.findObjectsInBackgroundWithBlock { (detailObj, NSError) -> Void in
            
            for detail in detailObj! {
                //self.detailObject = detail
                
                self.photoFile = detail.valueForKey("targetPhoto") as! PFFile
                
                self.photoFile.getDataInBackgroundWithBlock({ (imageData, NSError) -> Void in
                    
                    self.targetImageView.image = UIImage(data: imageData!)
                    
                    
                    }) { (percentDone: Int32) -> Void in
                        
                }
                
                self.targetNameLabel.text = detail.valueForKey("targetName") as? String
                
                let date = detail.valueForKey("dueDate") as? NSDate
                let formatter = NSDateFormatter()
                formatter.dateFormat = "dd MMM yyyy"
                formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
                self.dueDateLabel.text = "Due date : \(formatter.stringFromDate(date!))"
                
                let goalNumber = detail.valueForKey("goal") as! Double
                self.goalNumberLabel.text = "$\(goalNumber)"
                
                let currentlNumber = detail.valueForKey("current") as! Double
                self.currentNumberLabel.text = "$\(currentlNumber)"
                
                
                var remainNumber = 0.0
                
                remainNumber = goalNumber - currentlNumber
                self.remainNumberLabel.text = "$\(remainNumber)"
                
            }
            
            
        }
        
    }
    
    func setupAppearance() {
        
        let closeImg = UIImage(named: "ans_close_icon")!
        
        closeBtn = UIButton(frame: CGRectMake(screenSize.width * 0.865, screenSize.height * 0.085, closeImg.size.width * 1.35, closeImg.size.height * 1.35))
        closeBtn.setBackgroundImage(closeImg, forState: UIControlState.Normal)
        closeBtn.addTarget(self, action: #selector(ANSTargetDetailViewController.closeTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(closeBtn)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - ((appNameImg.size.width * 1.25)/2), screenSize.height * 0.07, appNameImg.size.width * 1.25, appNameImg.size.height * 1.25))
        appNameImageView.image = appNameImg
        self.view.addSubview(appNameImageView)
        
        
        targetBound = UIView(frame: CGRectMake(screenSize.width * 0.05, screenSize.height * 0.15, screenSize.width * 0.9, screenSize.height * 0.7))
        targetBound.layer.cornerRadius = 3.0
        targetBound.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8).CGColor
        targetBound.layer.borderWidth = 0.75
        self.view.addSubview(targetBound)
        
        targetImageView = UIImageView(frame: CGRectMake(0, 0, targetBound.frame.size.width, targetBound.frame.size.height * 0.5))
        targetImageView.contentMode = UIViewContentMode.ScaleAspectFill
        targetImageView.clipsToBounds = true
        targetBound.addSubview(targetImageView)
        
        targetNameLabel = UILabel(frame: CGRectMake(targetBound.frame.width * 0.05, targetImageView.frame.size.height - (targetImageView.frame.size.height * 0.25), targetBound.frame.size.width * 0.9, targetImageView.frame.size.height * 0.1))
        targetNameLabel.backgroundColor = UIColor.clearColor()
        targetNameLabel.textColor = UIColor.whiteColor()
        targetNameLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 16)
        targetNameLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        targetNameLabel.layer.shadowOpacity = 0.7
        targetNameLabel.layer.shadowRadius = 2
        targetImageView.addSubview(targetNameLabel)
        
        dueDateLabel = UILabel(frame: CGRectMake(targetBound.frame.width * 0.05, targetNameLabel.frame.origin.y + targetNameLabel.frame.size.height + (targetImageView.frame.size.height * 0.02), targetBound.frame.size.width * 0.9, targetImageView.frame.size.height * 0.1))
        dueDateLabel.backgroundColor = UIColor.clearColor()
        dueDateLabel.textColor = UIColor.whiteColor()
        dueDateLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        dueDateLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        dueDateLabel.layer.shadowOpacity = 0.7
        dueDateLabel.layer.shadowRadius = 2
        targetImageView.addSubview(dueDateLabel)
        
        let moreImage = UIImage(named: "ans_more_icon")!
        moreBtn = UIButton(frame: CGRectMake(targetBound.frame.width * 0.85, targetImageView.frame.origin.y + targetImageView.frame.size.height + (targetImageView.frame.size.height * 0.05), moreImage.size.width * 1.4, moreImage.size.height * 1.4))
        moreBtn.setBackgroundImage(moreImage, forState: UIControlState.Normal)
        moreBtn.addTarget(self, action: #selector(ANSTargetDetailViewController.moreTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        targetBound.addSubview(moreBtn)
        
        goalNumberLabel = UILabel(frame: CGRectMake(0, targetImageView.frame.origin.y + targetImageView.frame.size.height + (targetBound.frame.size.height * 0.055), targetBound.frame.size.width, targetBound.frame.size.height * 0.08))
        goalNumberLabel.backgroundColor = UIColor.clearColor()
        goalNumberLabel.textColor = UIColor.darkGrayColor()
        goalNumberLabel.textAlignment = NSTextAlignment.Center
        goalNumberLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.04)
        targetBound.addSubview(goalNumberLabel)
        
        goalLabel = UILabel(frame: CGRectMake(0, goalNumberLabel.frame.origin.y + goalNumberLabel.frame.size.height + (targetBound.frame.size.height * 0.02), targetBound.frame.size.width, targetBound.frame.size.height * 0.06))
        goalLabel.backgroundColor = UIColor.clearColor()
        goalLabel.textColor = UIColor.darkGrayColor()
        goalLabel.textAlignment = NSTextAlignment.Center
        goalLabel.text = "Goal"
        goalLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.02)
        targetBound.addSubview(goalLabel)
        
        
        endGoalLineView = UIView(frame: CGRectMake(0, goalLabel.frame.origin.y + goalLabel.frame.size.height + (targetBound.frame.size.height * 0.03), targetBound.frame.width, 1))
        endGoalLineView.backgroundColor =  UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8)
        targetBound.addSubview(endGoalLineView)
        
        
        currentNumberLabel = UILabel(frame: CGRectMake(0, endGoalLineView.frame.origin.y + endGoalLineView.frame.size.height + (targetBound.frame.size.height * 0.07), targetBound.frame.size.width/2, targetBound.frame.size.height * 0.08))
        currentNumberLabel.backgroundColor = UIColor.clearColor()
        currentNumberLabel.textColor = UIColor.darkGrayColor()
        currentNumberLabel.textAlignment = NSTextAlignment.Center
        currentNumberLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.035)
        targetBound.addSubview(currentNumberLabel)
        
        currentLabel = UILabel(frame: CGRectMake(0, currentNumberLabel.frame.origin.y + currentNumberLabel.frame.size.height, targetBound.frame.size.width/2, targetBound.frame.size.height * 0.06))
        currentLabel.backgroundColor = UIColor.clearColor()
        currentLabel.textColor = UIColor.darkGrayColor()
        currentLabel.textAlignment = NSTextAlignment.Center
        currentLabel.text = "Current"
        currentLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.017)
        targetBound.addSubview(currentLabel)
        
        
        halfLine = UIView(frame: CGRectMake(targetBound.frame.size.width/2, endGoalLineView.frame.origin.y + endGoalLineView.frame.size.height, 1, targetBound.frame.size.height - (endGoalLineView.frame.origin.y + endGoalLineView.frame.size.height)))
        halfLine.backgroundColor =  UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8)
        targetBound.addSubview(halfLine)
        
        
        remainNumberLabel = UILabel(frame: CGRectMake(targetBound.frame.size.width/2, endGoalLineView.frame.origin.y + endGoalLineView.frame.size.height + (targetBound.frame.size.height * 0.07), targetBound.frame.size.width/2, targetBound.frame.size.height * 0.08))
        remainNumberLabel.backgroundColor = UIColor.clearColor()
        remainNumberLabel.textColor = UIColor.darkGrayColor()
        remainNumberLabel.textAlignment = NSTextAlignment.Center
        remainNumberLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.035)
        targetBound.addSubview(remainNumberLabel)
        
        remainLabel = UILabel(frame: CGRectMake(targetBound.frame.size.width/2, currentNumberLabel.frame.origin.y + currentNumberLabel.frame.size.height, targetBound.frame.size.width/2, targetBound.frame.size.height * 0.06))
        remainLabel.backgroundColor = UIColor.clearColor()
        remainLabel.textColor = UIColor.darkGrayColor()
        remainLabel.textAlignment = NSTextAlignment.Center
        remainLabel.text = "Remaining"
        remainLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.017)
        targetBound.addSubview(remainLabel)
        
        
        addBtn = UIButton(frame: CGRectMake(screenSize.width * 0.05, targetBound.frame.origin.y + targetBound.frame.size.height + (screenSize.height * 0.045), screenSize.width * 0.9, screenSize.height * 0.07))
        addBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.025)
        addBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.8).CGColor
        addBtn.layer.cornerRadius = 2.0
        addBtn.layer.borderWidth = 1.2
        addBtn.addTarget(self, action: #selector(ANSTargetDetailViewController.addTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(addBtn)
        
        addLabel = UILabel(frame: CGRectMake(0, addBtn.frame.size.height * 0.3, addBtn.frame.size.width, addBtn.frame.size.height * 0.4))
        addLabel.textColor = UIColor.darkGrayColor()
        addLabel.textAlignment = NSTextAlignment.Center
        addLabel.text = "Saving History"
        addLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.02)
        addBtn.addSubview(addLabel)
        
    }
    
    func moreTapped(sender : UIButton) {
        
        
        let actionSheet = UIActionSheet(title: "Choose Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Edit", "Delete")
        
        actionSheet.showInView(self.view)
        
    }
    
    func addTapped(sender : UIButton) {
        let addSavingView = ANSAddSavingViewController();
        addSavingView.targetObjectID = self.targetID
        addSavingView.targetObject = self.targetObject
        self.presentViewController(addSavingView, animated: true) { () -> Void in
            
        }
    }
    
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int)
    {
        
        switch (buttonIndex){
            
        case 0:
            print("Cancel")
        case 1:
            print("Edit")
            
            let targetView = ANSAddTargetViewController();
            targetView.targetState = "Edit"
            targetView.targetID = self.targetID
            self.presentViewController(targetView, animated: true, completion: { () -> Void in
                
            })
            
        case 2:
            print("Delete")
            
            let savingQuery = PFQuery(className: "Saving")
            //savingQuery.includeKey("targetObj")
            savingQuery.whereKey("targetObj", equalTo: self.targetObject)
            savingQuery.findObjectsInBackgroundWithBlock({ (savings, NSError) -> Void in
                
                if savings?.count != 0 {
                    
                    for savingObject in savings! {
                        
                        savingObject.deleteInBackgroundWithBlock({ (sucess, NSError) -> Void in
                            
                            let targetQuery = PFQuery(className: "Target")
                            targetQuery.whereKey("objectId", equalTo: self.targetID)
                            targetQuery.findObjectsInBackgroundWithBlock({ (target, NSError) -> Void in
                                
                                for targetObj in target! {
                                    targetObj.deleteInBackgroundWithBlock({ (sucess, NSError) -> Void in
                                        
                                        
                                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                            
                                        })
                                        
                                    })
                                    
                                }
                                
                            })
                            
                        })
                        
                    }
                    
                }else{
                    
                    let targetQuery = PFQuery(className: "Target")
                    targetQuery.whereKey("objectId", equalTo: self.targetID)
                    targetQuery.findObjectsInBackgroundWithBlock({ (target, NSError) -> Void in
                        
                        for targetObj in target! {
                            targetObj.deleteInBackgroundWithBlock({ (sucess, NSError) -> Void in
                                
                                
                                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                    
                                })
                                
                            })
                            
                        }
                        
                    })
                    
                }
                
                
                
            })
            
            
        default:
            print("Default")
            //Some code here..
            
        }
    }
    
    
    func closeTapped(sender:UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
