//
//  ANSHistoryCell.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/17/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit

class ANSHistoryCell: UITableViewCell {
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var boundView: UIView!
    var moneyLabel: UILabel!
    var dateTimeLabel :UILabel!
    var actionBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }
    
    func setupView() {
        
        boundView = UIView(frame: CGRectMake(screenSize.width * 0.03, screenSize.height * 0.005, screenSize.width * 0.94, self.screenSize.height * 0.12))
        boundView.backgroundColor = UIColor.clearColor()
        boundView.layer.cornerRadius = 2.0
        boundView.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.7).CGColor
        boundView.layer.borderWidth = 0.75
        self.addSubview(boundView)
        
        
        moneyLabel = UILabel(frame: CGRectMake(boundView.frame.width * 0.1, boundView.frame.height * 0.2, boundView.frame.width * 0.4, self.screenSize.height * 0.08))
        moneyLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 25)
        moneyLabel.textColor = UIColor.darkGrayColor()
        moneyLabel.text = "$500.00"
        self.addSubview(moneyLabel)
        
        dateTimeLabel = UILabel(frame: CGRectMake(moneyLabel.frame.origin.x + moneyLabel.frame.size.width + (screenSize.width * 0.025), boundView.frame.height * 0.4, boundView.frame.width * 0.4, self.screenSize.height * 0.04))
        dateTimeLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 12)
        dateTimeLabel.textColor = UIColor.darkGrayColor()
        dateTimeLabel.text = "15/12/2015  14:50"
        self.addSubview(dateTimeLabel)
        
        
        let actionImg = UIImage(named: "ans_action_icon")!
        actionBtn = UIButton(frame: CGRectMake(boundView.frame.width * 0.95, boundView.frame.height * 0.42, actionImg.size.width, actionImg.size.height))
        actionBtn.setBackgroundImage(actionImg, forState: UIControlState.Normal)
        self.addSubview(actionBtn)
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
