//
//  ANSEditSavingViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/19/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

class ANSEditSavingViewController: UIViewController,
UITextFieldDelegate{
    
    var savingID: String!
    var savingObject: PFObject!
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var appNameImageView: UIImageView!
    var closeBtn: UIButton!
    var editView: UIView!
    var moneyTextField: UITextField!
    var cancelBtn: UIButton!
    var updateBtn: UIButton!
    var targetObjectId: String!
    var originalMoney: Double!
    
    override func viewWillAppear(animated: Bool) {
        self.fetchSavingObject()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupAppearance()
    }
    
    func fetchSavingObject() {
        
        var savingMoney: Double!
        var targetId: String!
        let query = PFQuery(className: "Saving")
        query.whereKey("objectId", equalTo: savingID)
        query.includeKey("targetObj")
        query.findObjectsInBackgroundWithBlock({ (savingObj, NSError) -> Void in
            
            for saving in savingObj! {
                savingMoney = saving.valueForKey("amount") as! Double
                
                self.moneyTextField.text = String(savingMoney)
                targetId = saving.valueForKey("targetObj")?.valueForKey("objectId") as! String
                
                
            }
            
            self.targetObjectId = targetId
            self.originalMoney = savingMoney
            
        })
    }
    
    func setupAppearance() {
        let closeImg = UIImage(named: "ans_close_icon")!
        
        closeBtn = UIButton(frame: CGRectMake(screenSize.width * 0.865, screenSize.height * 0.085, closeImg.size.width * 1.35, closeImg.size.height * 1.35))
        closeBtn.setBackgroundImage(closeImg, forState: UIControlState.Normal)
        closeBtn.addTarget(self, action: #selector(ANSEditSavingViewController.closeTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(closeBtn)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - ((appNameImg.size.width * 1.25)/2), screenSize.height * 0.07, appNameImg.size.width * 1.25, appNameImg.size.height * 1.25))
        appNameImageView.image = appNameImg
        self.view.addSubview(appNameImageView)
        
        editView = UIView(frame: CGRectMake(screenSize.width * 0.05, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width * 0.9, screenSize.height * 0.2))
        editView.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.7).CGColor
        editView.layer.borderWidth = 0.75
        editView.layer.cornerRadius = 4.0
        self.view.addSubview(editView)
        
        
        let moneyPlaceholder = NSAttributedString(string: "Enter your money...", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        
        moneyTextField = UITextField(frame: CGRectMake(editView.frame.width * 0.045, editView.frame.height * 0.09, editView.frame.width * 0.9, editView.frame.height * 0.35))
        moneyTextField.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.4)
        moneyTextField.attributedPlaceholder = moneyPlaceholder
        moneyTextField.textColor = UIColor.darkGrayColor()
        moneyTextField.textAlignment = NSTextAlignment.Center
        moneyTextField.delegate = self
        moneyTextField.layer.cornerRadius = 3.0
        moneyTextField.keyboardType = UIKeyboardType.DecimalPad
        moneyTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.03)
        editView.addSubview(moneyTextField)
        
        cancelBtn = UIButton(frame: CGRectMake(editView.frame.width * 0.13, moneyTextField.frame.origin.y + moneyTextField.frame.size.height + (screenSize.height * 0.02), moneyTextField.frame.width * 0.38, screenSize.height * 0.07))
        cancelBtn.setTitle("Cancel", forState: UIControlState.Normal)
        cancelBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.024)
        cancelBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.7).CGColor
        cancelBtn.layer.borderWidth = 1
        cancelBtn.layer.cornerRadius = 4.0
        cancelBtn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        cancelBtn.addTarget(self, action: #selector(ANSEditSavingViewController.cancelTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        editView.addSubview(cancelBtn)
        
        
        
        updateBtn = UIButton(frame: CGRectMake(cancelBtn.frame.origin.x + cancelBtn.frame.size.width + (editView.frame.width * 0.05), moneyTextField.frame.origin.y + moneyTextField.frame.size.height + (screenSize.height * 0.02), moneyTextField.frame.width * 0.38, screenSize.height * 0.07))
        updateBtn.setTitle("Update", forState: UIControlState.Normal)
        updateBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.024)
        updateBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.7).CGColor
        updateBtn.layer.borderWidth = 1
        updateBtn.layer.cornerRadius = 3
        updateBtn.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        updateBtn.addTarget(self, action: #selector(ANSEditSavingViewController.updateTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        editView.addSubview(updateBtn)
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(ANSEditSavingViewController.touchViewTapped(_:)))
        self.view.addGestureRecognizer(viewTap)
        
        
    }
    
    func touchViewTapped(sender : UITapGestureRecognizer) {
        
        moneyTextField!.resignFirstResponder()
        
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        
    }
    
    func cancelTapped(sender:UIButton) {
        
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
        
    }
    
    func updateTapped(sender:UIButton) {
        
        
        
        self.moneyTextField.resignFirstResponder()
        
        if moneyTextField.text != "" {
            
            self.view.showActivityViewWithLabel("Updating...")
            
            let currentQuery = PFQuery(className: "Target")
            currentQuery.whereKey("objectId", equalTo: self.targetObjectId)
            currentQuery.findObjectsInBackgroundWithBlock({ (currentObj, NSError) -> Void in
                for currentObject in currentObj! {
                    
                    var currentMoney = currentObject.valueForKey("current") as! Double
                    currentMoney = currentMoney - self.originalMoney
                    
                    currentObject["current"] = currentMoney
                    currentObject.saveInBackgroundWithBlock({ (success, NSError) -> Void in
                        
                        let query = PFQuery(className: "Saving")
                        query.whereKey("objectId", equalTo: self.savingID)
                        query.findObjectsInBackgroundWithBlock({ (saving, NSError) -> Void in
                            
                            for savingObj in saving!{
                                savingObj["amount"] = Double(self.moneyTextField.text!)
                                savingObj.saveInBackgroundWithBlock({ (success, NSError) -> Void in
                                    
                                    var currentMoney = currentObject.valueForKey("current") as! Double
                                    currentMoney = currentMoney + Double(self.moneyTextField.text!)!
                                    currentObject["current"] = currentMoney
                                    currentObject.saveInBackgroundWithBlock({ (sucess, NSError) -> Void in
                                        
                                        
                                        self.dismissViewControllerAnimated(true, completion: { () -> Void in
                                            self.view.hideActivityView()
                                        })
                                        
                                    })
                                    
                                })
                            }
                            
                        })
                        
                        
                        
                    })
                    
                }
            })
            
           
            
        }else{
            let popupView = self.createPopupview()
            
            self.presentPopupView(popupView)
        }
        
        
        
    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(20, 40, 160, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: screenSize.height * 0.018)
        popupLabel.text = "Please fill up your money."
        popupView.addSubview(popupLabel)
        
        // Close button
        //let button = UIButton.buttonWithType(.System)
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSEditSavingViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    
    
    
    func touchClose() {
        dismissPopupView()
    }

    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.moneyTextField.resignFirstResponder()
        
    }

    func closeTapped(sender:UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
