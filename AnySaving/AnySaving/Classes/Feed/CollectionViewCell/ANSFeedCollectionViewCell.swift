//
//  ANSFeedCollectionViewCell.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/13/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit

class ANSFeedCollectionViewCell: UICollectionViewCell {
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var photoImageView: UIImageView!
    var nameLabel: UILabel!
    var moneyLabel: UILabel!
    var currentLabel: UILabel!
    var fullBarView: UIView!
    var currentBarView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setupView()
    }
    
    func setupView() {
        
        self.frame = CGRectMake(0, 0, (screenSize.width/2) - 10, screenSize.height * 0.65)
        self.backgroundColor = UIColor.clearColor()
        self.layer.cornerRadius = 2.0
        self.layer.borderWidth = 0.75
        self.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1.0).CGColor
        
        photoImageView = UIImageView(frame: CGRectMake(0, 0, (screenSize.width/2) - 10, screenSize.height * 0.25))
        photoImageView.backgroundColor = UIColor.clearColor()
        photoImageView.contentMode = UIViewContentMode.ScaleAspectFill
        photoImageView.clipsToBounds = true
        self.addSubview(photoImageView)
        
        nameLabel = UILabel(frame: CGRectMake(screenSize.width * 0.05, photoImageView.frame.size.height * 0.8, photoImageView.frame.size.width * 0.8, photoImageView.frame.size.height * 0.15))
        nameLabel.text = "Car"
        nameLabel.backgroundColor = UIColor.clearColor()
        nameLabel.textAlignment = NSTextAlignment.Left
        nameLabel.textColor = UIColor.whiteColor()
        nameLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        nameLabel.layer.shadowOpacity = 0.7
        nameLabel.layer.shadowRadius = 2
        nameLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 17)
        photoImageView.addSubview(nameLabel)
        
        
        moneyLabel = UILabel(frame: CGRectMake(0, photoImageView.frame.origin.y + photoImageView.frame.size.height + (screenSize.height * 0.02), photoImageView.frame.size.width, photoImageView.frame.size.height * 0.15))
        moneyLabel.text = "$2,000"
        moneyLabel.backgroundColor = UIColor.clearColor()
        moneyLabel.textAlignment = NSTextAlignment.Center
        moneyLabel.textColor = UIColor.darkGrayColor()
        moneyLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 22)
        self.addSubview(moneyLabel)
        
        currentLabel = UILabel(frame: CGRectMake(0, moneyLabel.frame.origin.y + moneyLabel.frame.size.height, photoImageView.frame.size.width, photoImageView.frame.size.height * 0.15))
        currentLabel.text = "Current"
        currentLabel.backgroundColor = UIColor.clearColor()
        currentLabel.textAlignment = NSTextAlignment.Center
        currentLabel.textColor = UIColor.darkGrayColor()
        currentLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 13)
        self.addSubview(currentLabel)
        
        fullBarView = UIView(frame: CGRectMake(photoImageView.frame.size.width * 0.05, currentLabel.frame.origin.y + currentLabel.frame.size.height + (screenSize.height * 0.025), photoImageView.frame.size.width * 0.9, photoImageView.frame.size.height * 0.05))
        fullBarView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 1.0)
        self.addSubview(fullBarView)
        
        
        currentBarView = UIView(frame: CGRectMake(0, 0, fullBarView.frame.size.width * 0.3, fullBarView.frame.size.height))
        currentBarView.backgroundColor = UIColor(red: 140.0/255.0, green: 198.0/255.0, blue: 62.0/255.0, alpha: 1.0)
        fullBarView.addSubview(currentBarView)
        
        
        
    }

}
