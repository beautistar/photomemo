//
//  ANSFeedViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/12/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

class ANSFeedViewController: UIViewController,
UICollectionViewDelegate,
UICollectionViewDataSource{
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var addBtn : UIButton!
    @IBOutlet var feedCollectionView: UICollectionView!
    var feedList: NSMutableArray = []
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupAppearance()
    }
    
    override func viewWillAppear(animated: Bool) {
        let currentUser = PFUser.currentUser()
        
        if currentUser == nil {
            
            let loginView = ANSLoginViewController(nibName:"ANSLoginViewController",bundle:nil)

            self.presentViewController(loginView, animated: true, completion: { () -> Void in
                
            })
        }
        
        self.fetchFeedData()
        
        
    }

    func fetchFeedData() {
        
        let feedArray: NSMutableArray = []
        
        let feedQuery = PFQuery(className: "Target")
        feedQuery.includeKey("owner")
        feedQuery.whereKey("owner", equalTo: PFUser.currentUser()!)
        feedQuery.orderByDescending("createdAt")
        feedQuery.findObjectsInBackgroundWithBlock { (feedObj, NSError) -> Void in
            
            for feed in feedObj! {
                
                feedArray.addObject(feed)
                
            }
            
            self.feedList = feedArray
            self.feedCollectionView.reloadData()
            
        }
        
    }

    
    func setupAppearance() {
        
        let addImg = UIImage(named: "ans_add_icon")!
        addBtn = UIButton(frame: CGRectMake((screenSize.width/2) - (addImg.size.width/2), screenSize.height - (addImg.size.height + 20), addImg.size.width, addImg.size.height))
        addBtn.setBackgroundImage(addImg, forState: UIControlState.Normal)
        addBtn.addTarget(self, action: #selector(ANSFeedViewController.addTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(addBtn)
        
        //feedCollectionView = UICollectionView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
        feedCollectionView.backgroundColor = UIColor.clearColor()
        
        
        // this is just a test layout
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (screenSize.width/2) - 13, height: screenSize.height * 0.4)
        layout.scrollDirection = UICollectionViewScrollDirection.Vertical
        feedCollectionView.collectionViewLayout = layout
        
        // register the nib
        feedCollectionView.registerNib(UINib(nibName: "ANSFeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
        feedCollectionView.registerClass(ANSFeedHeaderCollectionReusableView.self,
            forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
            withReuseIdentifier: "HeaderView")
        
        feedCollectionView.delegate = self;
        feedCollectionView.dataSource = self;
        feedCollectionView.reloadData()
    
        self.view.addSubview(feedCollectionView)
        
    }
    
    
    func addTapped(sender:UIButton) {
        
        let addTargetView = ANSAddTargetViewController();
        addTargetView.targetState = "Add"
        self.presentViewController(addTargetView, animated: true) { () -> Void in
            
        }
        
    }
    
    func calculatePercent(money:Double) -> Double{
        
        return 0.0
        
    }
    
    // MARK: -  UICollectionViewDataSource
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        //return model.count
        return self.feedList.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        let cell:ANSFeedCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! ANSFeedCollectionViewCell
        
        if self.feedList.count != 0 {
            
            cell.nameLabel.text = self.feedList[indexPath.row].valueForKey("targetName") as? String
            
            let photos = self.feedList[indexPath.row].valueForKey("targetPhoto") as! PFFile
            
            photos.getDataInBackgroundWithBlock({ (imageData, NSError) -> Void in
                
                cell.photoImageView.image = UIImage(data: imageData!)
                
                
                }) { (percentDone: Int32) -> Void in
                    
            }
            
            let moneyNum = self.feedList[indexPath.row].valueForKey("current") as! Double
            cell.moneyLabel.text = "$\(String(moneyNum))"
            
            let goalNum = self.feedList[indexPath.row].valueForKey("goal") as! Double
            cell.currentBarView.frame = CGRectMake(cell.currentBarView.frame.origin.x, cell.currentBarView.frame.origin.y, (CGFloat(moneyNum) * cell.fullBarView.frame.size.width)/CGFloat(goalNum), cell.currentBarView.frame.size.height)
        
            
        }
        
        
        
        
        return cell
    }
    
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let targetDetailView = ANSTargetDetailViewController();
        targetDetailView.targetID = self.feedList[indexPath.row].valueForKey("objectId") as! String
        targetDetailView.targetObject = self.feedList[indexPath.row] as! PFObject
        self.presentViewController(targetDetailView, animated: true) { () -> Void in
            
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        
        /*
        switch kind {
            
        case UICollectionElementKindSectionHeader:
          */
            let headerView = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "HeaderView", forIndexPath: indexPath) as! ANSFeedHeaderCollectionReusableView
            
            for subview in headerView.subviews as [UIView] {
                
                if let labelView = subview as? UILabel {
                    labelView.removeFromSuperview()
                    
                } else {
                    
                }
            }
            
            headerView.backgroundColor = UIColor.clearColor()
            
            let logoutBtn = UIButton(frame: CGRectMake(screenSize.width * 0.025, screenSize.height * 0.055, screenSize.width * 0.2, screenSize.height * 0.05))
            logoutBtn.backgroundColor = UIColor.clearColor()
            logoutBtn.addTarget(self, action: #selector(ANSFeedViewController.logoutTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
            headerView.addSubview(logoutBtn)
            
            let logoutLabel = UILabel(frame: CGRectMake(0, 0, logoutBtn.frame.width, logoutBtn.frame.height))
            logoutLabel.text = "Logout"
            logoutLabel.textAlignment = NSTextAlignment.Left
            logoutLabel.textColor = UIColor.darkGrayColor()
            logoutLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
            logoutBtn.addSubview(logoutLabel)
            
            
            let appNameImg = UIImage(named: "ans_app_name")!
            let appNameImageView = UIImageView(frame: CGRectMake((self.feedCollectionView.frame.size.width/2) - ((appNameImg.size.width * 1.5)/2), screenSize.height * 0.08, appNameImg.size.width * 1.5, appNameImg.size.height * 1.5))
            appNameImageView.image = appNameImg
            headerView.addSubview(appNameImageView)
            
            
            let challengeLabel = UILabel(frame: CGRectMake(0, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.02), self.feedCollectionView.frame.size.width, screenSize.height * 0.04))
            challengeLabel.text = "Challenge yourself"
            challengeLabel.backgroundColor = UIColor.clearColor()
            challengeLabel.textAlignment = NSTextAlignment.Center
            challengeLabel.textColor = UIColor.darkGrayColor()
            challengeLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
            headerView.addSubview(challengeLabel)
            
            
            
            let numberLabel = UILabel(frame: CGRectMake(0, challengeLabel.frame.origin.y + challengeLabel.frame.size.height + (screenSize.height * 0.02), self.feedCollectionView.frame.size.width, screenSize.height * 0.08))
            numberLabel.text = String(self.feedList.count)
            numberLabel.backgroundColor = UIColor.clearColor()
            numberLabel.textAlignment = NSTextAlignment.Center
            numberLabel.textColor = UIColor.darkGrayColor()
            numberLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 44)
            headerView.addSubview(numberLabel)
            
            
            let goalLabel = UILabel(frame: CGRectMake(0, numberLabel.frame.origin.y + numberLabel.frame.size.height, self.feedCollectionView.frame.size.width, screenSize.height * 0.04))
            goalLabel.text = "Goals"
            goalLabel.backgroundColor = UIColor.clearColor()
            goalLabel.textAlignment = NSTextAlignment.Center
            goalLabel.textColor = UIColor.darkGrayColor()
            goalLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
            headerView.addSubview(goalLabel)

        
            return headerView
        
        /*
        default:
            
            assert(false, "Unexpected element kind")
            
        }*/
        
    }
    
    func collectionView(collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSizeMake(UIScreen.mainScreen().bounds.width, screenSize.height * 0.38)
    }
    
    func logoutTapped(sender:UIButton) {
        
        PFUser.logOut()
        
        let loginView = ANSLoginViewController();
        self.presentViewController(loginView, animated: true) { () -> Void in
            
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
