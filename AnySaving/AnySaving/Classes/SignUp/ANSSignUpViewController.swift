//
//  ANSSignUpViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/13/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse

class ANSSignUpViewController: UIViewController,
UITextFieldDelegate{
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var closeBtn : UIButton!
    var signUpBtn : UIButton!
    var underLineSignUp: UIView!
    var appIconImageView : UIImageView!
    var appNameImageView: UIImageView!
    var welcomeLabel : UILabel!
    var emailTextField: UITextField!
    var passwordTextField: UITextField!
    var signUpLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupAppearance()
    }
    
    
    func setupAppearance() {
        
        let closeImg = UIImage(named: "ans_close_icon")!
        
        closeBtn = UIButton(frame: CGRectMake(screenSize.width * 0.865, screenSize.height * 0.085, closeImg.size.width * 1.35, closeImg.size.height * 1.35))
        closeBtn.setBackgroundImage(closeImg, forState: UIControlState.Normal)
        closeBtn.addTarget(self, action: #selector(ANSSignUpViewController.closeTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(closeBtn)
        
        let appIconImg = UIImage(named: "Logo")!
        let imageSize  = CGSizeMake(90, 90)        
        
        appIconImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - (imageSize.width/2), screenSize.height * 0.07, imageSize.width, imageSize.height))
        appIconImageView.image = appIconImg
        self.view.addSubview(appIconImageView)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - (appNameImg.size.width/2), appIconImageView.frame.origin.y + appIconImageView.frame.size.height + (screenSize.height * 0.03), appNameImg.size.width, appNameImg.size.height))
        appNameImageView.image = appNameImg
        self.view.addSubview(appNameImageView)
        
        welcomeLabel = UILabel(frame: CGRectMake(0, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width, screenSize.height * 0.05))
        welcomeLabel.backgroundColor = UIColor.clearColor()
        welcomeLabel.text = "Sign Up for Free"
        welcomeLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        welcomeLabel.textColor = UIColor.darkGrayColor()
        welcomeLabel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(welcomeLabel)
        
        
        let emailPlaceholder = NSAttributedString(string: "Email Address", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        emailTextField = UITextField(frame: CGRectMake(screenSize.width * 0.1, welcomeLabel.frame.origin.y + welcomeLabel.frame.size.height + (screenSize.height * 0.04), screenSize.width * 0.8, screenSize.height * 0.08))
        emailTextField.backgroundColor = UIColor.clearColor()
        emailTextField.attributedPlaceholder = emailPlaceholder
        emailTextField.textColor = UIColor.darkGrayColor()
        emailTextField.textAlignment = NSTextAlignment.Center
        emailTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        emailTextField.layer.borderWidth = 0.75
        emailTextField.layer.cornerRadius = 3.0
        emailTextField.delegate = self
        emailTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(emailTextField)
        
        let passwordPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        passwordTextField = UITextField(frame: CGRectMake(screenSize.width * 0.1, emailTextField.frame.origin.y + emailTextField.frame.size.height + (screenSize.height * 0.015), screenSize.width * 0.8, screenSize.height * 0.08))
        passwordTextField.backgroundColor = UIColor.clearColor()
        passwordTextField.attributedPlaceholder = passwordPlaceholder
        passwordTextField.textColor = UIColor.darkGrayColor()
        passwordTextField.textAlignment = NSTextAlignment.Center
        passwordTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        passwordTextField.layer.borderWidth = 0.75
        passwordTextField.layer.cornerRadius = 3.0
        passwordTextField.delegate = self
        passwordTextField.secureTextEntry = true
        passwordTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(passwordTextField)
        
        
        signUpBtn = UIButton(frame: CGRectMake(screenSize.width * 0.1, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + (screenSize.height * 0.02), screenSize.width * 0.8, screenSize.height * 0.07))
        signUpBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        //signInBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        signUpBtn.layer.cornerRadius = 3.0
        signUpBtn.layer.borderWidth = 0.75
        signUpBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        signUpBtn.addTarget(self, action: #selector(ANSSignUpViewController.signUpTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signUpBtn.backgroundColor = UIColor.blackColor()
        self.view.addSubview(signUpBtn)
        
        signUpLabel = UILabel(frame: CGRectMake(0, signUpBtn.frame.size.height * 0.25, signUpBtn.frame.size.width, signUpBtn.frame.size.height * 0.45))
        signUpLabel.text = "Sign Up"
        signUpLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        signUpLabel.textColor = UIColor.whiteColor()
        signUpLabel.textAlignment = NSTextAlignment.Center
        signUpBtn.addSubview(signUpLabel)
        
        
        
        
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(ANSSignUpViewController.touchViewTapped(_:)))
        self.view.addGestureRecognizer(viewTap)
        
        
        
    }
    
    
    func touchViewTapped(sender : UITapGestureRecognizer) {
        
        emailTextField!.resignFirstResponder()
        passwordTextField!.resignFirstResponder()
        
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        
    }
    
    func signUpTapped(sender:UIButton) {
        
        if emailTextField.text != "" && passwordTextField.text != "" {
            
            let user = PFUser()
            user.username = emailTextField.text
            user.password = passwordTextField.text
            
            user.signUpInBackgroundWithBlock {
                (succeeded: Bool, error: NSError?) -> Void in
                if (error != nil) {
                    print("Error")
                    
                    let popupView = self.createPopupview()
                    
                    self.emailTextField?.resignFirstResponder()
                    self.passwordTextField?.resignFirstResponder()
                    
                    self.presentPopupView(popupView)
            
                    
                } else {
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        
                    })
                }
            }
            
        }else{
            
            let popupView = self.createPopupview()
            
            self.emailTextField?.resignFirstResponder()
            self.passwordTextField?.resignFirstResponder()
            
            self.presentPopupView(popupView)
        }
        
    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(20, 40, 160, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont.systemFontOfSize(16)
        popupLabel.text = "Please fill up all field."
        popupView.addSubview(popupLabel)
        
        // Close button
        //let button = UIButton.buttonWithType(.System)
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSSignUpViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    
    
    
    func touchClose() {
        dismissPopupView()
    }
    
    
    func closeTapped(sender:UIButton) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == emailTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
        if textField == passwordTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
        
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        emailTextField!.resignFirstResponder()
        passwordTextField!.resignFirstResponder()
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
