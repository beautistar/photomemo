//
//  ANSLoginViewController.swift
//  AnySaving
//
//  Created by Saengrawi Sapmoon on 12/13/2558 BE.
//  Copyright © 2558 Saengrawi Sapmoon. All rights reserved.
//

import UIKit
import Parse
import ParseFacebookUtilsV4
import FBSDKCoreKit
import ParseTwitterUtils


class ANSLoginViewController: UIViewController,
UITextFieldDelegate{
    
    var screenSize: CGRect = UIScreen.mainScreen().bounds
    var signInBtn : UIButton!
    var signUpBtn : UIButton!
    var underLineSignUp: UIView!
    var appIconImageView : UIImageView!
    var appNameImageView: UIImageView!
    var hintLabel : UILabel!
    var welcomeLabel : UILabel!
    var emailTextField: UITextField!
    var passwordTextField: UITextField!
    var signInLabel: UILabel!
    var signUpLabel: UILabel!
    
    var signInFacebook : UIButton!
    var signInTwitter  : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupAppearance()
    }
    
    func setupAppearance() {
        
        let appIconImg = UIImage(named: "Logo")!
        let imageSize  = CGSizeMake(90, 90)
        appIconImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - (imageSize.width/2), screenSize.height * 0.07, imageSize.width, imageSize.height))
        appIconImageView.image = appIconImg
        self.view.addSubview(appIconImageView)
        
        let appNameImg = UIImage(named: "ans_app_name")!
        appNameImageView = UIImageView(frame: CGRectMake((screenSize.width/2) - (appNameImg.size.width/2), appIconImageView.frame.origin.y + appIconImageView.frame.size.height + (screenSize.height * 0.03), appNameImg.size.width, appNameImg.size.height))
        appNameImageView.image = appNameImg
//        self.view.addSubview(appNameImageView)
        
        hintLabel = UILabel();
        hintLabel.textColor = UIColor.darkGrayColor()
        hintLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        hintLabel.numberOfLines = 2;
        hintLabel.textAlignment = NSTextAlignment.Center
        hintLabel.text = "MEMO AND PHOTO FOR WHAT\nEVER YOU NEED TO USE"
        hintLabel.frame = CGRectMake(0, appIconImageView.frame.maxY + 20, screenSize.width, 50)
        self.view.addSubview(hintLabel)
        
        
        welcomeLabel = UILabel(frame: CGRectMake(0, appNameImageView.frame.origin.y + appNameImageView.frame.size.height + (screenSize.height * 0.03), screenSize.width, screenSize.height * 0.05))
        welcomeLabel.backgroundColor = UIColor.clearColor()
        welcomeLabel.text = "Welcome User"
        welcomeLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        welcomeLabel.textColor = UIColor.darkGrayColor()
        welcomeLabel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(welcomeLabel)
        
        
        let emailPlaceholder = NSAttributedString(string: "Email Address", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        emailTextField = UITextField(frame: CGRectMake(screenSize.width * 0.1, welcomeLabel.frame.origin.y + welcomeLabel.frame.size.height + (screenSize.height * 0.04), screenSize.width * 0.8, screenSize.height * 0.08))
        emailTextField.backgroundColor = UIColor.clearColor()
        emailTextField.attributedPlaceholder = emailPlaceholder
        emailTextField.textColor = UIColor.darkGrayColor()
        emailTextField.textAlignment = NSTextAlignment.Center
        emailTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        emailTextField.layer.borderWidth = 0.75
        emailTextField.layer.cornerRadius = 3.0
        emailTextField.delegate = self
        emailTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(emailTextField)
        
        let passwordPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName : UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100.0/255.0, alpha: 0.5)])
        passwordTextField = UITextField(frame: CGRectMake(screenSize.width * 0.1, emailTextField.frame.origin.y + emailTextField.frame.size.height + (screenSize.height * 0.015), screenSize.width * 0.8, screenSize.height * 0.08))
        passwordTextField.backgroundColor = UIColor.clearColor()
        passwordTextField.attributedPlaceholder = passwordPlaceholder
        passwordTextField.textColor = UIColor.darkGrayColor()
        passwordTextField.textAlignment = NSTextAlignment.Center
        passwordTextField.layer.borderColor = UIColor.lightGrayColor().CGColor
        passwordTextField.layer.borderWidth = 0.75
        passwordTextField.layer.cornerRadius = 3.0
        passwordTextField.delegate = self
        passwordTextField.secureTextEntry = true
        passwordTextField.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 14)
        self.view.addSubview(passwordTextField)
        
        
        signInBtn = UIButton(frame: CGRectMake(screenSize.width * 0.1, passwordTextField.frame.origin.y + passwordTextField.frame.size.height + (screenSize.height * 0.02), screenSize.width * 0.8, screenSize.height * 0.07))
        signInBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        //signInBtn.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        signInBtn.layer.cornerRadius = 3.0
        signInBtn.layer.borderWidth = 0.75
        signInBtn.layer.borderColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1).CGColor
        signInBtn.addTarget(self, action: #selector(ANSLoginViewController.signInTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signInBtn.backgroundColor = UIColor.blackColor()
        self.view.addSubview(signInBtn)
        
        signInLabel = UILabel(frame: CGRectMake(0, signInBtn.frame.size.height * 0.25, signInBtn.frame.size.width, signInBtn.frame.size.height * 0.45))
        signInLabel.text = "Sign In"
        signInLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        signInLabel.textColor = UIColor.whiteColor()
        signInLabel.textAlignment = NSTextAlignment.Center
        signInBtn.addSubview(signInLabel)
        
        
        let dt : CGFloat = screenSize.height * 0.02
        signUpBtn = UIButton(frame: CGRectMake(screenSize.width * 0.35, signInBtn.frame.origin.y + signInBtn.frame.size.height + dt, screenSize.width * 0.3, screenSize.height * 0.07))
        signUpBtn.addTarget(self, action: #selector(ANSLoginViewController.signUpTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signUpBtn.titleLabel?.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        //signUpBtn.setTitleColor(UIColor.darkGrayColor(), forState: UIControlState.Normal)
        self.view.addSubview(signUpBtn)
        
        signUpLabel = UILabel(frame: CGRectMake(0, signUpBtn.frame.size.height * 0.25, signUpBtn.frame.size.width, signUpBtn.frame.size.height * 0.45))
        signUpLabel.text = "Sign Up"
        signUpLabel.font = UIFont(name: "Hiragino Kaku Gothic ProN", size: 15)
        signUpLabel.textColor = UIColor.darkGrayColor()
        signUpLabel.textAlignment = NSTextAlignment.Center
        signUpBtn.addSubview(signUpLabel)
        
        underLineSignUp = UIView(frame: CGRectMake(screenSize.width * 0.375, signUpBtn.frame.origin.y + (signUpBtn.frame.size.height - 4), screenSize.width * 0.25, 1))
        underLineSignUp.backgroundColor = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
        self.view.addSubview(underLineSignUp)
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(ANSLoginViewController.touchViewTapped(_:)))
        self.view.addGestureRecognizer(viewTap)
        
        
        let width : CGFloat = (signInBtn.frame.size.width - dt) / 2
        let height : CGFloat = signInBtn.frame.size.height
        let rectFacebookButton : CGRect = CGRectMake(signInBtn.frame.origin.x, CGRectGetMaxY(signUpBtn.frame) + dt, width, height)
        signInFacebook = UIButton(frame: rectFacebookButton)
        self.view.addSubview(signInFacebook)
        signInFacebook.setImage(UIImage(named: "button_facebook"), forState: UIControlState.Normal)
        signInFacebook.backgroundColor = UIColor(colorLiteralRed: 59.0/255.0, green: 89.0/255.0, blue: 152.0/255.0, alpha: 1)
        signInFacebook.addTarget(self, action: #selector(ANSLoginViewController.signinWithFacebook(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signInFacebook.layer.cornerRadius = 3;
        
        let rectTwitterButton : CGRect = CGRectMake(CGRectGetMaxX(rectFacebookButton) + dt, rectFacebookButton.origin.y, width, height);
        signInTwitter = UIButton(frame: rectTwitterButton)
        self.view.addSubview(signInTwitter)
        signInTwitter.setImage(UIImage(named: "button_twitter"), forState: UIControlState.Normal)
        signInTwitter.backgroundColor = UIColor(colorLiteralRed: 85.0/255.0, green: 172.0/255.0, blue: 238.0/255.0, alpha: 1)
        signInTwitter.addTarget(self, action: #selector(ANSLoginViewController.signinWithTwitter(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        signInTwitter.layer.cornerRadius = 3;
        
    }
    
    func signUpTapped(sender : UIButton) {
        let signUpView = ANSSignUpViewController();
        self.presentViewController(signUpView, animated: true) { () -> Void in
            
        }
    }
    
    func signInTapped(sender : UIButton) {
        
        if emailTextField.text != "" && passwordTextField.text != "" {
            
            
            PFUser.logInWithUsernameInBackground(emailTextField.text!, password:passwordTextField.text!) {
                (user: PFUser?, error: NSError?) -> Void in
                if user != nil {
                    
                    self.gotoFeedView()
                    
                } else {
                    let popupView = self.createLoginFailPopupview()
                    
                    self.emailTextField?.resignFirstResponder()
                    self.passwordTextField?.resignFirstResponder()
                    
                    self.presentPopupView(popupView)
                }
            }
            
            
            
        }else{
            
            let popupView = self.createPopupview()
            
            self.emailTextField?.resignFirstResponder()
            self.passwordTextField?.resignFirstResponder()
            
            self.presentPopupView(popupView)
        }
        
    }
    
    func gotoFeedView() {
        let feedView = ANSFeedViewController();
        self.presentViewController(feedView, animated: true, completion: { () -> Void in
            
        })

    }
    
    func createPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(20, 40, 160, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont.systemFontOfSize(16)
        popupLabel.text = "Please fill up all field."
        popupView.addSubview(popupLabel)
        
        // Close button
        //let button = UIButton.buttonWithType(.System)
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSLoginViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    func createLoginFailPopupview() -> UIView {
        
        let popupView = UIView(frame: CGRectMake(0, 0, 200, 160))
        popupView.backgroundColor = UIColor.blackColor()
        
        
        //Text
        let popupLabel = UILabel(frame: CGRectMake(20, 40, 160, 21))
        popupLabel.backgroundColor = UIColor.clearColor()
        popupLabel.textColor = UIColor.whiteColor()
        popupLabel.textAlignment = NSTextAlignment.Center
        popupLabel.font = UIFont.systemFontOfSize(16)
        popupLabel.text = "Login Fail!"
        popupView.addSubview(popupLabel)
        
        // Close button
        let button = UIButton(frame: CGRectMake(60, 90, 80, 35))
        //button.frame = CGRectMake(60, 90, 80, 35)
        button.setTitle("Close", forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(ANSLoginViewController.touchClose), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.backgroundColor = UIColor.whiteColor()
        button.layer.cornerRadius = 18.0
        popupView.addSubview(button)
        
        return popupView
    }
    
    
    func touchClose() {
        dismissPopupView()
    }
    
    
    func touchViewTapped(sender : UITapGestureRecognizer) {
        
        emailTextField!.resignFirstResponder()
        passwordTextField!.resignFirstResponder()
        
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        
    }
   
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if textField == emailTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
        if textField == passwordTextField {
            self.view.frame = CGRectMake(0, -150, screenSize.width, screenSize.height)
        }
        
    
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        emailTextField!.resignFirstResponder()
        passwordTextField!.resignFirstResponder()
        self.view.frame = CGRectMake(0, 0, screenSize.width, screenSize.height)
        
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func signinWithFacebook(sender : UIButton) -> Void {
        print("facebook login")
        let permissions = ["email", "public_profile"]
        PFFacebookUtils.logInInBackgroundWithReadPermissions(permissions) { (user: PFUser?, error: NSError?) in
            if let user = user {
                
                self.getUserInfo()
                
                if user.isNew {
                    
                } else {
                    
                }
                
                self.gotoFeedView()
                
            } else {
                
            }
        }
    }
    
    func signinWithTwitter(sender : UIButton) -> Void {
        print("twitter login")
        
        PFTwitterUtils.logInWithBlock {
            (user: PFUser?, error: NSError?) -> Void in
            if let user = user {
                if user.isNew {
                    print("User signed up and logged in with Twitter!")
                } else {
                    print("User logged in with Twitter!")
                }
                
                self.gotoFeedView()
                
            } else {
                print("Uh oh. The user cancelled the Twitter login.")
            }
        }
    }
    
    func getUserInfo() {
        // Create request for user's Facebook data
        let request = FBSDKGraphRequest(graphPath:"me", parameters:nil)
        
        // Send request to Facebook
        request.startWithCompletionHandler {
            
            (connection, result, error) in
            
            if error != nil {
                // Some error checking here
            }
            else if let userData = result as? [String:AnyObject] {
                
                // Access user data
                let username = userData["name"] as? String
                print(username)
                print(userData)
                // ....
            }
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
